﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class LogSpeed : MonoBehaviour
{
    private Rigidbody m_rb;

    private void Start()
    {
        m_rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Debug.Log("Speed: " + m_rb.velocity.magnitude);
    }
}
