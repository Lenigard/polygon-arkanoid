﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class AchievementAsset : ScriptableObject
{	
	[Serializable]
	public struct ProgressStep
	{
		public int StepValue;
		public int RewardValue;
	}
	
	[SerializeField] private string m_id;
	[SerializeField] private string m_name;
	[SerializeField] private string m_description;
	[SerializeField] private ProgressStep[] m_progressSteps;

	[Header("Debug:")] 
	[SerializeField] private int m_saveValue;
	
	public string Id
	{
		get { return m_id; }
		set { m_id = value; }
	}
	
	public string Name
	{
		get { return m_name; }
		set { m_name = value; }
	}

	public string Description
	{
		get { return m_description; }
		set { m_description = value; }
	}

	public ProgressStep[] ProgressSteps
	{
		get { return m_progressSteps; }
		set { m_progressSteps = value; }
	}

	public int CurrProgress
	{
		get { return Mathf.Clamp(PlayerPrefs.GetInt(m_id), 0, MaxReqValue); }
		set { PlayerPrefs.SetInt(m_id, value); }
	}

	public int MaxReqValue
	{
		get
		{
			if (m_progressSteps == null || m_progressSteps.Length == 0)
			{
				return 0;
			}
			
			return m_progressSteps[m_progressSteps.Length - 1].StepValue;
		}
	}
	
	public int CurrReqProgress
	{
		get
		{
			if (m_progressSteps == null || m_progressSteps.Length == 0)
			{
				return 0;
			}

			foreach (var step in ProgressSteps)
			{
				if (CurrProgress < step.StepValue)
				{
					return step.StepValue;
				}
			}

			return MaxReqValue;
		}
	}

	public int CompletedStars
	{
		get
		{
			if (m_progressSteps == null || m_progressSteps.Length == 0)
			{
				return 0;
			}

			var stars = 0;
			foreach (var step in ProgressSteps)
			{
				if (CurrProgress >= step.StepValue)
				{
					stars++;
					continue;
				}
				
				break;
			}

			return stars;
		}
	}

	public string GetProgressSting()
	{
		return CurrProgress + "/" + CurrReqProgress;
	}
	
	public static void SaveProgress(string id, int value)
	{
		if (PlayerPrefs.HasKey(id))
		{
			PlayerPrefs.SetInt(id, value );
		}
	}
	
	[ContextMenu("Save debug progress")]
	public void SaveDebugProgress()
	{
		CurrProgress = m_saveValue;
		Debug.Log("Save");
	}
}