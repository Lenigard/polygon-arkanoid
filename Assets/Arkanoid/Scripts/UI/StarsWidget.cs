﻿using UnityEngine;
using UnityEngine.UI;

public class StarsWidget : MonoBehaviour
{
	[SerializeField] private IntVariable m_starsCount;
	
	[Space]
	[SerializeField] private Image[] m_stars;
	[SerializeField] private Sprite m_emptyStar;
	[SerializeField] private Sprite m_filledStar;

	private void Start()
	{
		SetStars(m_starsCount.Value);

		m_starsCount.OnValueChanged += SetStars;
	}

	private void OnDestroy()
	{
		m_starsCount.OnValueChanged -= SetStars;
	}

	private void SetStars(int count)
	{
		for (int i = 0; i < m_stars.Length; i++)
		{
			var starSprite = i < count ? m_filledStar : m_emptyStar;
			m_stars[i].sprite = starSprite;
		}
	}
	
}