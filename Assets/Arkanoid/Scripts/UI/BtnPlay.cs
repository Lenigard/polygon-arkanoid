﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BtnPlay : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnBtnClick);
    }

    private void OnDestroy()
    {
        GetComponent<Button>().onClick.RemoveListener(OnBtnClick);
    }

    private void OnBtnClick()
    {
        if(CustomInput.ShotAction != null)
        {
            CustomInput.ShotAction.Invoke();
        }
    }
}
