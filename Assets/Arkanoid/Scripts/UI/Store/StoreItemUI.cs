﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode]
public class StoreItemUI : MonoBehaviour
{
	[SerializeField] private StoreItem m_storeItemAsset;
	
	[Header("References:")]
	[SerializeField] private TextMeshProUGUI m_nameTmp;
	[SerializeField] private TextMeshProUGUI m_valueTmp;
	[SerializeField] private TextMeshProUGUI m_priceTmp;
	[SerializeField] private Image m_icon;
	[SerializeField] private Button m_buyBtn;

	private void Start()
	{
		if (m_buyBtn != null)
		{
			m_buyBtn.onClick.AddListener(OnClick);
		}
	}

	private void OnDestroy()
	{
		if (m_buyBtn != null)
		{
			m_buyBtn.onClick.RemoveListener(OnClick);
		}
	}

	private void Update()
	{
		if (m_storeItemAsset == null)
		{
			return;
		}

		ApplyAsset(m_storeItemAsset);
	}

	private void ApplyAsset(StoreItem asset)
	{
		m_nameTmp.text = asset.Name;
		m_valueTmp.text = "+" + asset.Value;
		m_priceTmp.text = asset.Price + "$";

		m_icon.sprite = asset.Icon;
	}

	private void OnClick()
	{
		m_storeItemAsset.Buy();
		Debug.Log(UserWallet.GetValue(ECurrency.Coin));
	}
}