﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SelectInputWidget : MonoBehaviour
{
	[SerializeField] private InputSettings m_settings;
	[SerializeField] private TMP_Dropdown m_dropdown;

	private void Start()
	{
		var options = new List<TMP_Dropdown.OptionData>();

		foreach (var inputType in Enum.GetValues(typeof(EInputType)))
		{
			options.Add(new TMP_Dropdown.OptionData(inputType.ToString()));
		}
		
		m_dropdown.AddOptions(options);
		m_dropdown.value = (int)m_settings.CurrInput;
		
		m_dropdown.onValueChanged.AddListener(OnValueChanged);
	}

	private void OnDestroy()
	{
		m_dropdown.onValueChanged.RemoveListener(OnValueChanged);
	}

	private void OnValueChanged(int id)
	{
		m_settings.CurrInput = (EInputType) id;
	}
}