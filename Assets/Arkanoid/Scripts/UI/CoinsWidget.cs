﻿using UnityEngine;
using TMPro;

public class CoinsWidget : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI m_valueTmp;
	
	private void Start()
	{
		RefreshValue();
		UserWallet.OnItemChanged += OnCoinsChanged;
	}

	private void OnDestroy()
	{
		UserWallet.OnItemChanged -= OnCoinsChanged;
	}

	private void OnCoinsChanged(ECurrency currency)
	{
		if (currency == ECurrency.Coin)
		{
			RefreshValue();
		}
	}

	private void RefreshValue()
	{
		m_valueTmp.text = UserWallet.GetValue(ECurrency.Coin).ToString();
	}
}