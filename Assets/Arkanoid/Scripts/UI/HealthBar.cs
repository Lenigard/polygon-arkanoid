﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image[] m_hearts;

    private void Start()
    {
        SetHealth(GameController.PlayerHealth);
    }

    private void Update()
    {
        SetHealth(GameController.PlayerHealth);
    }

    private void SetHealth(int health)
    {
        for(int i = 0; i < m_hearts.Length; i++)
        {
            m_hearts[i].enabled = i < health;
        }
    }
}
