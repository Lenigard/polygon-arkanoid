﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ACOpenablePanel : AOpenablePanel
{
	[SerializeField] private Animator m_animator;

	public void OpenClose()
	{
		if (IsOpened)
		{
			Close();
		}
		else
		{
			Open();
		}
	}
	
	protected override void OpenAnim()
	{
		if (!IsOpened)
		{
			m_animator.SetTrigger("Open");
		}
	}

	protected override void CloseAnim()
	{
		if (IsOpened)
		{
			m_animator.SetTrigger("Close");
		}
	}
}