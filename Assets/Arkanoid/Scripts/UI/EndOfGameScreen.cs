﻿using UnityEngine;

public class EndOfGameScreen : MonoBehaviour
{
    [SerializeField] private GameObject m_winScreen;
    [SerializeField] private GameObject m_loseScreen;
    [SerializeField] private GameObject m_shadow;
    [SerializeField] private Animator m_anim;

    private void Start()
    {
        m_winScreen.SetActive(false);
        m_loseScreen.SetActive(false);
        m_shadow.SetActive(false);

        GameController.EndOfGameEvent += OnEndOfGame;
    }

    private void OnDestroy()
    {
        GameController.EndOfGameEvent -= OnEndOfGame;
    }

    private void OnEndOfGame(bool isVictory)
    {
        m_winScreen.SetActive(isVictory);
        m_loseScreen.SetActive(!isVictory);
        m_shadow.SetActive(true);

        m_anim.SetTrigger("Open");
    }
}
