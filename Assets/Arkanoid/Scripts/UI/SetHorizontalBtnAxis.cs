﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
public class SetHorizontalBtnAxis : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	[SerializeField] private InputSettings m_settings;
	[SerializeField] [Range(-1, 1)] private float m_axis;

	public UnityEvent OnPointerDownEvent = new UnityEvent();
	public UnityEvent OnPointerUpEvent = new UnityEvent();
	
	private bool m_isPressed;

	private void Start()
	{
		SetInputType();
		m_settings.OnInputTypeChanged.AddListener(SetInputType);
	}

	private void OnDestroy()
	{
		m_settings.OnInputTypeChanged.RemoveListener(SetInputType);
	}

	private void Update()
	{
		CustomInput.HorizontalAxis = m_isPressed ? m_axis : 0;	
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		m_isPressed = true;
		OnPointerDownEvent.Invoke();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		m_isPressed = false;
		OnPointerUpEvent.Invoke();
	}

	private void SetInputType()
	{
		gameObject.SetActive(m_settings.CurrInput == EInputType.UIButtons);
	}
}