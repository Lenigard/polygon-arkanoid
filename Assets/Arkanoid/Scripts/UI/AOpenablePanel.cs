﻿using UnityEngine;

public abstract class AOpenablePanel : MonoBehaviour
{
	public bool IsOpened { get; private set; }
	
	protected abstract void OpenAnim();
	protected abstract void CloseAnim();

	public void Open()
	{
		OpenAnim();
		
		IsOpened = true;
	}

	public void Close()
	{
		CloseAnim();
		
		IsOpened = false;
	}
}