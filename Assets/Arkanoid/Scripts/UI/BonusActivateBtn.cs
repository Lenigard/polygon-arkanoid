﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode]
[RequireComponent((typeof(Button)))]
public class BonusActivateBtn : MonoBehaviour
{

	[SerializeField] private ABonusBtnAsset m_asset;
	
	[Header("References:")]
	[SerializeField] private Image m_icon;
	[SerializeField] private Image m_cooldownImg;
	[SerializeField] private TextMeshProUGUI m_priceTmp;

	private void Start()
	{
		ApplyAsset();

		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	private void OnDestroy()
	{
		GetComponent<Button>().onClick.RemoveListener(OnClick);
	}

	private void OnClick()
	{
		if (m_asset.CooldownNormalized > 0)
		{
			return;
		}

		var userValue = UserWallet.GetValue(m_asset.Currency);
		if (userValue < m_asset.Price)
		{
			return;
		}
		
		UserWallet.SetValue(m_asset.Currency, userValue -  m_asset.Price);
		m_asset.Activate();
	}

	private void Update()
	{
		#if UNITY_EDITOR
		ApplyAsset();
		#endif

		if (Application.isPlaying)
		{
			m_asset.UpdateCooldown(Time.deltaTime);
			ApplyCooldown();
		}
	}
	
	private void ApplyAsset()
	{
		if (m_asset == null || m_icon == null || m_priceTmp == null)
		{
			return;
		}

		m_icon.sprite = m_asset.Icon;
		m_priceTmp.text = m_asset.Price.ToString();
	}

	private void ApplyCooldown()
	{
		m_cooldownImg.fillAmount = m_asset.CooldownNormalized;
	}
}