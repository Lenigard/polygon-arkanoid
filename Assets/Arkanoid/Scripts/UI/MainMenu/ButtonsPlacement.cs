﻿using UnityEngine;

public class ButtonsPlacement : MonoBehaviour
{

    [SerializeField] private LoadLevelBtn m_prefab;
    [SerializeField] private int m_numOfLevels;


    [ContextMenu("Place buttons")]
    public void PlaceButtons()
    {
	    while (transform.childCount > 0)	    
	    {
		    DestroyImmediate(transform.GetChild(0).gameObject);
	    }
	    
	    for (int i = 0; i < m_numOfLevels; i++)
	    {
		    var instance = Instantiate(m_prefab, transform);
		    instance.LevelNum = i + 1;
		    instance.SetStars(2);
		    instance.SetLevelNumTMP(instance.LevelNum);
		    instance.name = "LevelBtn_" + instance.LevelNum;
		    instance.gameObject.SetActive(true);
	    }
    }
}
