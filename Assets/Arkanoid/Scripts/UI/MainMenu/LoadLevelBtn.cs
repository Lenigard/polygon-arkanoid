﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class LoadLevelBtn : MonoBehaviour
{

	public int LevelNum;
	
	[SerializeField] private TextMeshProUGUI m_levelTmp;
	[SerializeField] private Image[] m_stars;
	
	private void Start()
	{
		SetStars(StarsStorage.GetStars(LevelNum));
		
		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	private void OnDestroy()
	{
		GetComponent<Button>().onClick.RemoveListener(OnClick);
	}

	public void SetStars(int count)
	{
		var activeStar = Resources.Load<Sprite>("activeStar");
		var inactiveStar = Resources.Load<Sprite>("inactiveStar");
		
		for (int i = 0; i < m_stars.Length; i++)
		{
			m_stars[i].sprite = i < count ? activeStar : inactiveStar;
		}
	}

	public void SetLevelNumTMP(int num)
	{
		if (m_levelTmp != null)
		{
			m_levelTmp.text = num.ToString();
		}
	}
	
	private void OnClick()
	{
		if (LevelNum < SceneManager.sceneCountInBuildSettings - 1 )
		{
			SceneManager.LoadSceneAsync(LevelNum);
		}
	}
}