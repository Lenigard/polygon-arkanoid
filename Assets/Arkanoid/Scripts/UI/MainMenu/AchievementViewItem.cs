﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using  TMPro;

[ExecuteInEditMode]
public class AchievementViewItem : MonoBehaviour
{
	[SerializeField] private AchievementAsset m_asset;
	
	[Header("References:")]
	[SerializeField] private TextMeshProUGUI m_nameTmp;
	[SerializeField] private TextMeshProUGUI m_descriptionTmp;
	[SerializeField] private TextMeshProUGUI m_progressTmp;
	[SerializeField] private Image[] m_stars;

	private void Start()
	{
		ApplyAsset(m_asset);
	}

	#if UNITY_EDITOR
	private void Update()
	{
		if (m_asset != null)
		{
			ApplyAsset(m_asset);
		}
	}
	#endif

	private void ApplyAsset(AchievementAsset asset)
	{
		SetName(asset.Name);
		SetDescription(asset.Description);
		SetProgress(asset.GetProgressSting());
		SetNumOfStars(asset.ProgressSteps.Length);
		SetCompletedStars(asset.CompletedStars);
	}
	
	private void SetName(string achName)
	{
		if (m_nameTmp != null)
		{
			m_nameTmp.text = achName;
		}
	}
	
	private void SetDescription(string achDesc)
	{
		if (m_descriptionTmp != null)
		{
			m_descriptionTmp.text = achDesc;
		}
	}
	
	private void SetProgress(string progressStr)
	{
		if (m_progressTmp != null)
		{
			m_progressTmp.text = progressStr;
		}
	}

	private void SetNumOfStars(int numOfStars)
	{
		for (var i = 0; i < m_stars.Length; i++)
		{
			m_stars[i].gameObject.SetActive(i < numOfStars);
		}
	}

	private void SetCompletedStars(int completedStars)
	{
		var activeStar = Resources.Load<Sprite>("activeStar");
		var inactiveStar = Resources.Load<Sprite>("inactiveStar");
		
		for (var i = 0; i < m_stars.Length; i++)
		{
			if (!m_stars[i].gameObject.activeSelf)
			{
				break;
			}

			m_stars[i].sprite = i < completedStars ? activeStar : inactiveStar;
		}
	}
}