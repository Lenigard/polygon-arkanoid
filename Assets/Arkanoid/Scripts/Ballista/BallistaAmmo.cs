﻿using UnityEngine;
public class BallistaAmmo : MonoBehaviour
{
    [SerializeField] private float m_damage = 1f;
    [SerializeField] private GameObject m_fx;

    private void OnCollisionEnter(Collision collision)
    {
        var health = collision.collider.GetComponent<ObjectHealth>();

        if (health != null)
        {
            health.Health -= m_damage;
        }

        if (m_fx != null)
        {
            var obj = Instantiate(m_fx, transform.position, transform.rotation);
            Destroy(obj, 1.5f);
        }

        Destroy(gameObject);
    }
}
