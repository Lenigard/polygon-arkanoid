﻿using UnityEngine;

public class BallistaAiming : MonoBehaviour
{
    [Header("References:")]
    [SerializeField] private Transform m_tower;

    [Header("Settings:")]
    [SerializeField] private float rotationSpeed = 1.5f;
    [SerializeField] private float m_minAngle = 0.9f;

    private GameObject m_currTarget;

    public GameObject Target
    {
        get { return m_currTarget; }
        set { m_currTarget = value; }
    }

    private void Update()
    {
        if (m_currTarget == null)
        {
            FindClosestEnemyTarget();
            return;
        }

        RotateTower(m_tower);
    }

    private void RotateTower(Transform tower)
    {
        var lookDir = m_currTarget.transform.position - tower.position;
        lookDir.y = 0;

        var targetRot = Quaternion.LookRotation(lookDir);
        var rotSpeed = rotationSpeed * Time.deltaTime;
        tower.rotation = Quaternion.Slerp(tower.rotation, targetRot, rotSpeed);
    }

    private void FindClosestEnemyTarget()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (m_currTarget != null)
        {
            m_currTarget = enemies[0];
        }

        var smallsestDist = float.MaxValue;

        foreach (var enemy in enemies)
        {
            var distance = (transform.position - enemy.transform.position).sqrMagnitude;

            if (distance < smallsestDist)
            {
                smallsestDist = distance;
                m_currTarget = enemy;
            }
        }
    }

    public bool IsAimed()
    {
        if (Target == null)
        {
            return false;
        }

        var ditToTarget = (Target.transform.position - m_tower.position).normalized;
        ditToTarget.y = 0;
        return Vector3.Dot(ditToTarget, m_tower.forward) >= m_minAngle;
    }
}
