﻿using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class SetBallistaTargetByClick : MonoBehaviour
{
    [SerializeField] private LayerMask m_mask;
    private BallistaAiming m_ballistaAiming;

    private void Start()
    {
        m_ballistaAiming = GetComponent<BallistaAiming>();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 50f, m_mask.value))
            {
                m_ballistaAiming.Target = hit.transform.gameObject;
            }
        }
    }
}
