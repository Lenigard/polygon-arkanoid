﻿using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class BallisatAimEffect : MonoBehaviour
{
    [SerializeField] private Transform m_aimEffect;
    [SerializeField] private float m_movementSpeed = 5f;

    private BallistaAiming m_ballistaAiming;

    private void Start()
    {
        Debug.Log("csd");
        m_ballistaAiming = GetComponent<BallistaAiming>();
    }

    private void Update()
    {                                                                                                  
        var targetExist = m_ballistaAiming.Target != null;

        m_aimEffect.gameObject.SetActive(targetExist);

        if (!targetExist)
        {
            return;
        }

        var newPos = Vector3.Lerp(m_aimEffect.position, m_ballistaAiming.Target.transform.position, Time.deltaTime * m_movementSpeed);
        newPos.y = m_aimEffect.position.y;
        m_aimEffect.position = newPos;
    }
}
