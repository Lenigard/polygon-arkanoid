﻿using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class BallistaShooting : MonoBehaviour
{
	[Header("References:")] 
	[SerializeField] private GameObject m_ammo;
	[SerializeField] private Transform m_firePoint;
	[SerializeField] private LayerMask m_shootingMask;
	
	[Header("Shot Settings:")] 
	[SerializeField] private float m_shotForce = 300;
	[SerializeField] private float m_shootingFrequency = 1.0f;
	
	private BallistaAiming m_ballistaAiming;
	private float m_shootingTimer;
	
	private void Start()
	{
		m_ballistaAiming = GetComponent<BallistaAiming>();

		ResetTimer();
	}

	private void Update()
	{
		UpdateTimer();

		if (m_ballistaAiming.Target == null) return;
        if (!m_ballistaAiming.IsAimed()) return;

		if (m_shootingTimer <= 0)
		{
			RaycastHit hit;

            var raycastDir = (m_ballistaAiming.Target.transform.position - m_firePoint.position).normalized;

            if (Physics.Raycast(m_firePoint.position, raycastDir, out hit, 50f, m_shootingMask.value))
			{
				if (!hit.collider.CompareTag("Enemy")) return;

				MakeShot();
				ResetTimer();
			}
		}
	}

	private void ResetTimer()
	{
		m_shootingTimer = m_shootingFrequency;
	}

	private void UpdateTimer()
	{
		m_shootingTimer -= Time.deltaTime;
	}

	private void MakeShot()
	{
		PlayShotAnim();
		var shootPos = m_firePoint.position;
		var shootRotation = m_firePoint.rotation;

		var ammoClone = Instantiate(m_ammo, shootPos, shootRotation);
		var rb = ammoClone.GetComponent<Rigidbody>();

		rb.isKinematic = false;
		rb.AddForce(m_firePoint.forward * m_shotForce, ForceMode.Impulse);
		Destroy(ammoClone, 3f);
	}

	private void PlayShotAnim()
	{
		var anim = gameObject.GetComponent<Animator>();
		if (anim != null) anim.SetTrigger("Shot");
	}
}