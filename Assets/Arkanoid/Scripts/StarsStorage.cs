﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "StarsStorage", fileName = "NewStarsStorage")]
public class StarsStorage : ScriptableObject
{
	[SerializeField] private int m_levelNum;
	[SerializeField] private int m_starsNum;
	
	private static string m_key = "LevelStars";
	
	public static int GetStars(int levelNum)
	{
		return PlayerPrefs.GetInt(m_key + levelNum);
	}
	
	public static void SetStars(int levelNum, int starsNum)
	{
		PlayerPrefs.SetInt(m_key + levelNum, starsNum);
	}

	[ContextMenu("Set stars")]
	public void SetStars()
	{
		SetStars(m_levelNum, m_starsNum);
		Debug.Log("Set stars. Curr count: " + GetStars(m_levelNum));
	}
}