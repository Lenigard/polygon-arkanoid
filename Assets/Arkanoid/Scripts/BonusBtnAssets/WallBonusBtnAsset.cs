﻿using UnityEngine;

[CreateAssetMenu(menuName = "BonusBtnAsset/Wall", fileName = "NewWallBonusBtnAsset")]
public class WallBonusBtnAsset : ABonusBtnAsset
{
	[SerializeField] private EWall m_type;

	public override void Activate()
	{
		base.Activate();

		var wallsPlacement = FindObjectOfType<WallsPlacement>();
		if (wallsPlacement == null)
		{
			Debug.LogWarning("There is no any WallsPlacement in the scene");
			return;
		}

		switch (m_type)
		{
			case EWall.Wood:
			{
				wallsPlacement.PlaceAllWoodWalls();
				break;
			}
			case EWall.Stone:
			{
				wallsPlacement.PlaceAllStoneWalls();
				break;
			}
			case EWall.Steel:
			{
				wallsPlacement.PlaceAllSteelWalls();
				break;
			}
		}
	}
}