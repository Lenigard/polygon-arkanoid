﻿using UnityEngine;

public abstract class ABonusBtnAsset : ScriptableObject
{
	[SerializeField] private float m_cooldown;
	private float m_cooldownNormalized;

	[SerializeField] private float m_duration;
	[SerializeField] private Sprite m_icon;
	[SerializeField] private ECurrency m_currency;
	[SerializeField] private int m_price;

	public Sprite Icon
	{
		get { return m_icon; }
	}

	public ECurrency Currency
	{
		get { return m_currency; }
	}

	public int Price
	{
		get { return m_price; }
	}

	public float Duration
	{
		get { return m_duration; }
	}

	public float Cooldown { get; private set; }

	public float CooldownNormalized
	{
		get
		{
			if (m_cooldown <= 0)
			{
				return 0;
			}
			
			return Cooldown / m_cooldown;
		}
	}

	public virtual void Activate()
	{
		Cooldown = m_cooldown;
	}

	public void UpdateCooldown(float dt)
	{
		Cooldown = Mathf.Max(0, Cooldown - dt);
	}
}