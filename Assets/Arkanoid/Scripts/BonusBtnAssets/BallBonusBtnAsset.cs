﻿using UnityEngine;

[CreateAssetMenu(menuName = "BonusBtnAsset/Ball", fileName = "NewBallBonusBtnAsset")]
public class BallBonusBtnAsset : ABonusBtnAsset
{
	[SerializeField] private BallType m_type;

	public override void Activate()
	{
		base.Activate();
		
		var platform = GameObject.FindGameObjectWithTag("Platform");
		if (platform == null)
		{
			Debug.LogWarning("There is no any Platform in the scene");
			return;
		}
		
		var bonus = platform.AddComponent<BonusBall>();
		bonus.Duration = Duration;
		bonus.BallType = m_type;
	}
}