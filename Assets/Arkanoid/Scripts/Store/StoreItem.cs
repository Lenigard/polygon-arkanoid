﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class StoreItem : ScriptableObject
{
	[SerializeField] private string m_name;
	[SerializeField] private Sprite m_icon;
	[SerializeField] private ECurrency m_currency;
	[SerializeField] private int m_value;
	[SerializeField] private float m_price;

	public string Name
	{
		get { return m_name; }
	}

	public Sprite Icon
	{
		get { return m_icon; }
	}

	public ECurrency Currency
	{
		get { return m_currency; }
	}

	public int Value
	{
		get { return m_value; }
	}

	public float Price
	{
		get { return m_price; }
	}

	public bool Buy()
	{
		var userValue = UserWallet.GetValue(m_currency);
		UserWallet.SetValue(m_currency, userValue + m_value);
		return true;
	}
}