﻿using System.Collections;
using UnityEngine;

public class CoroutineExample : MonoBehaviour
{
    public SpriteRenderer m_renderer;

    private void Start()
    {
        //StartCoroutine(Example01_DelayedCall());
        //StartCoroutine(Example02_RepeadedCall());
        //StartCoroutine(Example03_WaitUntil());
        //StartCoroutine(Example04_EndlessLoop());
        //StartCoroutine(Example05_EndlessLoopAndBreak());
        //StartCoroutine(Example06_Parametraised(3));
        //StartCoroutine(Example07_WWW());
        //StartCoroutine(Example08_MultipleCoroutines());


        StartCoroutine(FadeIn(m_renderer, 0.01f));
    }

    //private IEnumerator Start()
    //{
    //    var coroutine = StartCoroutine(Example01_DelayedCall());
    //    yield return null;

    //    //StopCoroutine(coroutine);
    //    //Destroy(gameObject);
    //}

    private IEnumerator Example01_DelayedCall()
    {
        Debug.Log("I am waiting for 2 seconds. Time: " + Time.time);

        yield return new WaitForSeconds(2f);

        Debug.Log("After 2 seconds. Time: " + Time.time);
    }

    private IEnumerator Example02_RepeadedCall()
    {
        var delay = new WaitForSeconds(0.5f);
        var counter = 5;

        while (counter > 0)
        {
            yield return delay;

            Debug.Log(counter-- + " Time: " + Time.time);
        }
    }

    private IEnumerator Example03_WaitUntil()
    {
        Debug.Log("Start waiting!");

        var delay = new WaitUntil(() => Input.GetKeyDown(KeyCode.Space));

        yield return delay;

        Debug.Log("Stop waiting!");
    }

    private IEnumerator Example04_EndlessLoop()
    {
        while (true)
        {
            yield return null;

            Debug.Log("Endless Loop");
        }
    }

    private IEnumerator Example05_EndlessLoopAndBreak()
    {
        while (true)
        {
            yield return null;

            Debug.Log("Loop");

            if (Input.GetKeyDown(KeyCode.Space))
            {
                break;
            }
        }

        Debug.Log("Stop!");
    }

    private IEnumerator Example06_Parametraised(float delay)
    {
        Debug.Log("Start: " + Time.time);

        yield return new WaitForSeconds(delay);

        Debug.Log("Stop: " + Time.time);
    }

    private IEnumerator Example07_WWW()
    {
        var request = new WWW("https://www.google.com/");

        yield return request;

        Debug.Log(request.text);
    }

    private IEnumerator Example08_MultipleCoroutines()
    {
        yield return StartCoroutine(Example01_DelayedCall());
        yield return StartCoroutine(Example05_EndlessLoopAndBreak());
        yield return StartCoroutine(Example07_WWW());
    }

    public IEnumerator FadeIn(SpriteRenderer renderer, float delay)
    {
        var mDelay = new WaitForSeconds(delay);
        var clr = renderer.color;

        while(clr.a > 0)
        {
            clr.a -= 0.01f;
            renderer.color = clr;

            yield return mDelay;
        }
    }
}
