﻿using System;
using UnityEngine;

public abstract class AVariable<T> : ScriptableObject
{
	[SerializeField] private T m_value;

	public Action<T> OnValueChanged;

	public T Value
	{
		get { return m_value; }
		set
		{
			m_value = value;

			if (OnValueChanged != null) OnValueChanged.Invoke(m_value);
		}
	}
}

[CreateAssetMenu(menuName = "SOVariable/Int", fileName = "NewInt")]
public class IntVariable : AVariable<int>
{
}

[CreateAssetMenu(menuName = "SOVariable/Float", fileName = "NewFloat")]
public class FloatVariable : AVariable<float>
{
}