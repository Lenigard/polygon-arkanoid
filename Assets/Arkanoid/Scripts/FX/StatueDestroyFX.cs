﻿using UnityEngine;

public class StatueDestroyFX : MonoBehaviour
{
    [SerializeField] private Rigidbody[] m_parts;
    [SerializeField] private float m_impulse;
    [SerializeField] private float m_destroyDelay;
    [SerializeField] private float m_fallDownDelay;

    private bool m_fallDown;

    private void Start()
    {
        Play();
        Destroy(gameObject, m_destroyDelay);
    }

    private void Update()
    {
        m_destroyDelay -= Time.deltaTime;
        m_fallDownDelay -= Time.deltaTime;

        if(m_fallDownDelay <= 0)
        {
            FallDown();
        }
    }

    private void FallDown()
    {
        if (!m_fallDown)
        {
            m_fallDown = true;

            foreach (var part in m_parts)
            {
                part.GetComponent<Collider>().isTrigger = true;
            }
        }
    }

    private void Play()
    {
        foreach(var part in m_parts)
        {
            var dir = part.transform.forward;

            part.isKinematic = false;
            part.AddForce(dir * m_impulse, ForceMode.Impulse);
            part.AddForce(transform.forward * m_impulse, ForceMode.Impulse);
        }
    }
}
