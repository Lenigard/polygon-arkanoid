﻿public class BonusChangeSpeed : ABonus
{
    public float SpeedModificator = 1f;

    protected override void Activate()
    {
        var platformMovement = GetComponent<APlatformMovement>();

        if(platformMovement != null)
        {
            platformMovement.CurrSpeed = platformMovement.BaseSpeed * SpeedModificator;
            UnityEngine.Debug.Log(SpeedModificator);
        }
    }

    protected override void Deactivate()
    {
        var platformMovement = GetComponent<APlatformMovement>();

        if (platformMovement != null)
        {
            platformMovement.CurrSpeed = platformMovement.BaseSpeed;
        }
    }
}
