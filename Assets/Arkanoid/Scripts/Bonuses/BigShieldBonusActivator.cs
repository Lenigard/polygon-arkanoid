﻿using UnityEngine;

public class BigShieldBonusActivator : ABonusActivator
{
    public override void Activate(GameObject target)
    {
        var bonus = target.AddComponent<BonusBigShield>();
        bonus.Duration = m_duration;
    }
}
