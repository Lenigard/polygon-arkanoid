﻿using UnityEngine;

public class ChangeSpeedBonusActivator : ABonusActivator
{
    [SerializeField] private float m_speedModificator = 1f;

    public override void Activate(GameObject target)
    {
        var bonus = target.AddComponent<BonusChangeSpeed>();
        UnityEngine.Debug.Log("Activate");

        bonus.Duration = m_duration;
        bonus.SpeedModificator = m_speedModificator;
    }
}
