using  UnityEngine;

public class FingerInput : IInput
{
	private float m_prevMousePosX;
	private float m_sensitivity = 50;
	
	public void UpdateInput()
	{
		if (Input.GetMouseButtonDown(0))
		{
			m_prevMousePosX = Input.mousePosition.x;
		}
		else if (Input.GetMouseButton(0))
		{
			var mouseDelta = (Input.mousePosition.x - m_prevMousePosX) / Screen.width * m_sensitivity;
			m_prevMousePosX = Input.mousePosition.x;
			CustomInput.HorizontalAxis = mouseDelta;
		}
		else
		{
			CustomInput.HorizontalAxis = 0;
		}
	}
}