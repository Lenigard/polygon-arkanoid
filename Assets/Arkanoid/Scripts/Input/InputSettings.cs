﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "InputManager/Settings", fileName = "NewInputSettings")]
public class InputSettings : ScriptableObject
{
	[SerializeField] private EInputType m_currType;

	public UnityEvent OnInputTypeChanged = new UnityEvent();

	public EInputType CurrInput
	{
		get { return m_currType; }
		set
		{
			if (m_currType != value)
			{
				m_currType = value;
				OnInputTypeChanged.Invoke();
			}
		}
	}
}