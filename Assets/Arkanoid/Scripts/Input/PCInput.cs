﻿using UnityEngine;

public class PCInput : IInput
{
    public void UpdateInput()
    {
        CustomInput.HorizontalAxis = 0;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            CustomInput.HorizontalAxis = -1;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            CustomInput.HorizontalAxis = 1;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(CustomInput.ShotAction != null)
            {
                CustomInput.ShotAction.Invoke();
            }
        }
    }
}
