﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private InputSettings m_settings;
    
    private IInput m_input;

    private void Start()
    {
        SetInputType();
        
        m_settings.OnInputTypeChanged.AddListener(SetInputType);
    }

    private void OnDestroy()
    {
        m_settings.OnInputTypeChanged.RemoveListener(SetInputType);
    }

    private void Update()
    {
        if(m_input != null)
        {
            m_input.UpdateInput();
        }
    }

    private void SetInputType()
    {
        switch (m_settings.CurrInput)
        {
            case EInputType.Keyboard:
                m_input = new PCInput();
                break;
            case EInputType.Accelerometer:
                m_input = new AccelerometerInput();
                break;
            case EInputType.Finger:
                m_input = new FingerInput();
                break;
            default:
                m_input = null;
                break;
        }
    }
}
