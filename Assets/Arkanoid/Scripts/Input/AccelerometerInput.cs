﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccelerometerInput : IInput
{
    public void UpdateInput()
    {
        CustomInput.HorizontalAxis = Input.acceleration.x;
    }
}
