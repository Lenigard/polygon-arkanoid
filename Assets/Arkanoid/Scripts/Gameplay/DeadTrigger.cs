﻿using UnityEngine;

public class DeadTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            GameController.PlayerHealth--;
            GameController.StopGame = true;
        }
    }
}
