﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private Vector3 m_movementDir = Vector3.back;
    [SerializeField] private float m_movementSpeed = 5;

    private void Update()
    {
        transform.Translate(m_movementDir * m_movementSpeed * Time.deltaTime);
    }
}
