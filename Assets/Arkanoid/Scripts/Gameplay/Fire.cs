﻿using System.Collections;
using UnityEngine;

public class Fire : MonoBehaviour
{
    [SerializeField] private GameObject m_fireFx;

    private bool m_inFire;

    private void Start()
    {
        PlayFireFx(false);
    }

    public void EnableFire()
    {
        if (m_inFire)
        {
            return;
        }

        StartCoroutine(FireProcess());
    }

   private IEnumerator FireProcess ()
    { 
        PlayFireFx(true);
        m_inFire = true;

        var delay = new WaitForSeconds(1f);

        while (true)
        {       
            yield return delay;
 

            DoDamage();
            SetFireAround();
        }
    }

    private void DoDamage()
    {
        var health = GetComponent<ObjectHealth>();

        if(health != null)
        {
            health.Health -= 1;
        }
    }

    private void SetFireAround()
    {
        var hits = Physics.OverlapSphere(transform.position, 4f);

        foreach(var collider in hits)
        {
            if(collider.gameObject == this.gameObject)
            {
                continue;
            }

            var fire = collider.gameObject.GetComponent<Fire>();

            if(fire != null)
            {
                fire.EnableFire();
            }
        }
    }

    private void PlayFireFx(bool state)
    {
        if(m_fireFx != null)
        {
            m_fireFx.SetActive(state);
        }
    }
}
