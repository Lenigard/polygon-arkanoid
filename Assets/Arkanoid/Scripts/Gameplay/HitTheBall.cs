﻿using UnityEngine;

public class HitTheBall : MonoBehaviour
{
    [SerializeField] private Animator m_animator;

    private void PlayAnim()
    {
        m_animator.SetTrigger("Hit");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            var rb = other.GetComponent<Rigidbody>();

            if(rb != null)
            {
                var ballForward = rb.velocity.normalized;
                var dot = Vector3.Dot(ballForward, transform.forward);

                if(dot < 0)
                {
                    PlayAnim();
                }
            }        
        }
    }
}
