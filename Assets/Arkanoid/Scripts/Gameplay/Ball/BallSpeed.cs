﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallSpeed : MonoBehaviour
{
    [SerializeField] private float m_targetSpeed = 15f;

    private Rigidbody m_rb;

    public float CurrSpeed
    {
        get { return m_targetSpeed; }
        set { m_targetSpeed = value; }
    }

    void Start()
    {
        m_rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float sqrSpeed = m_rb.velocity.sqrMagnitude;

        if(sqrSpeed != Mathf.Pow(m_targetSpeed, 2))
        {
            m_rb.velocity = m_rb.velocity.normalized * m_targetSpeed;
        }
    }
}
