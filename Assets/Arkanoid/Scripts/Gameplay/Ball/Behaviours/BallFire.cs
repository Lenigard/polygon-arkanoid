﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewBallKamikadze", menuName = "BallSettings/BallFire")]
public class BallFire : BallBehaviourAsset
{
    public override void EnableBehaviour(GameObject ball)
    {
        base.EnableBehaviour(ball);

        var attack = ball.GetComponent<BallAttack>();

        if (attack != null)
        {
            attack.OnDamage.AddListener(OnDamage);
        }
    }

    public override void DisableBehaviour(GameObject ball)
    {
        base.DisableBehaviour(ball);

        var attack = ball.GetComponent<BallAttack>();

        if (attack != null)
        {
            attack.OnDamage.RemoveListener(OnDamage);
        }
    }

    private void OnDamage(GameObject target)
    {
        var fire = target.GetComponent<Fire>();

        if (fire != null)
        {
            fire.EnableFire();
        }
    }
}

