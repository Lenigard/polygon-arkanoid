﻿using UnityEngine;

public class ChangeBallTypeBtn : MonoBehaviour
{
    [SerializeField] private BallType m_type;

    public void ChangeBallType()
    {
        var allBalls = GameObject.FindObjectsOfType<BallBehaviourExecuter>();

        foreach(var ball in allBalls)
        {
            ball.SetBallType(m_type);
        }
    }
}
