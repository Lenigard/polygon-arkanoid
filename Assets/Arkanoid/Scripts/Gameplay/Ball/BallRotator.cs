﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallRotator : MonoBehaviour
{
	[SerializeField] private float m_rotSpeed = 50f;
	[SerializeField] private Transform m_rotatePointUp;
	[SerializeField] private Transform m_rotatePointRight;
	
	private Rigidbody m_rb;

	private void Start()
	{
		m_rb = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		var ballSpeed = m_rb.velocity.magnitude;
		
		if (ballSpeed > 0)
		{
			var point = m_rotatePointUp.position;
			var around = m_rotatePointUp.right;
			var speed = ballSpeed * Time.deltaTime * m_rotSpeed;
			var look = m_rb.velocity;
			look.y = 0;

			m_rotatePointUp.rotation = Quaternion.LookRotation(look);
			m_rotatePointRight.RotateAround(point, around, speed);
		}
	}
}