﻿using System.Collections;
using UnityEngine;

public class WallsPlacement : MonoBehaviour
{
    [SerializeField] private float m_placementDelay;
    [SerializeField] private Transform m_woodWalls;
    [SerializeField] private Transform m_stoneWalls;
    [SerializeField] private Transform m_steelWalls;

    Coroutine m_coroutine;

    private void Start()
    {
        DisableWalls(m_woodWalls);
        DisableWalls(m_stoneWalls);
        DisableWalls(m_steelWalls);
    }

    public void PlaceWoodWall()
    {
        PlaceWall(m_woodWalls);
    }

    public void PlaceStoneWall()
    {
        PlaceWall(m_stoneWalls);
    }

    public void PlaceSteelWall()
    {
        PlaceWall(m_steelWalls);
    }

    public void PlaceAllWoodWalls()
    {
        if(m_coroutine != null)
        {
            StopCoroutine(m_coroutine);
        }

        m_coroutine = StartCoroutine(PlaceAllWalls(m_woodWalls));
    }

    public void PlaceAllStoneWalls()
    {
        if (m_coroutine != null)
        {
            StopCoroutine(m_coroutine);
        }

        m_coroutine = StartCoroutine(PlaceAllWalls(m_stoneWalls));
    }

    public void PlaceAllSteelWalls()
    {
        if (m_coroutine != null)
        {
            StopCoroutine(m_coroutine);
        }

        m_coroutine = StartCoroutine(PlaceAllWalls(m_steelWalls));
    }

    private bool PlaceWall(Transform root)
    {
        foreach (Transform child in root)
        {
            if (!child.gameObject.activeSelf)
            {
                var canPlace =
                    CanPlaceWall(child, m_woodWalls) &&
                    CanPlaceWall(child, m_stoneWalls) &&
                    CanPlaceWall(child, m_steelWalls);

                if (canPlace)
                {
                    child.gameObject.SetActive(true);
                    return true;
                }
            }
        }

        return false;
    }

    private bool CanPlaceWall(Transform wall, Transform root)
    {
        foreach(Transform childWall in root)
        {
            if (childWall.gameObject.activeSelf)
            {
                if(Vector3.Distance(wall.position, childWall.position) < 0.1f)
                {
                    return false;
                }
            }
        }

        return true;
    }

    private void DisableWalls(Transform root)
    {
        foreach(Transform child in root)
        {
            child.gameObject.SetActive(false);
        }
    }

    private IEnumerator PlaceAllWalls(Transform root)
    {
        var delay = new WaitForSeconds(m_placementDelay);

        while (true)
        {
            var result = PlaceWall(root);
            yield return delay;

            if(result == false)
            {
                yield break;
            }
        }
    }
}
