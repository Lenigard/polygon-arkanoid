﻿using UnityEngine;

public class ShieldSwitcher : MonoBehaviour
{
    [Header("Base shield:")]
    [SerializeField] private GameObject m_shield;
    [SerializeField] private GameObject m_shieldCollider;

    [Header("Big shield:")]
    [SerializeField] private GameObject m_bigShield;
    [SerializeField] private GameObject m_bigShieldColider;

    private void Start()
    {
        SwitchShield(false);
    }

    public void SwitchShield(bool isBig)
    {
        m_shield.gameObject.SetActive(!isBig);
        m_shieldCollider.gameObject.SetActive(!isBig);

        m_bigShield.gameObject.SetActive(isBig);
        m_bigShieldColider.gameObject.SetActive(isBig);
    }
}
