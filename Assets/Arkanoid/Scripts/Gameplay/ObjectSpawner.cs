﻿using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] m_prefabs;

    public void Spawn()
    {
        foreach(var prefab in m_prefabs)
        {
            Instantiate(prefab, transform.position, transform.rotation);
        }
    }
}
