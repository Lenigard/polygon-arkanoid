﻿using System.Collections;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    [SerializeField] private float m_startDelay = 5f;
    [SerializeField] private float m_spawnFrequency = 15f;

    [Header("References:")]  
    [SerializeField] private GameObject m_prefab;
    [SerializeField] private ParticleSystem m_spawnFX;
    [SerializeField] private Transform m_spawnPlace;

    private void Start()
    {
        StartCoroutine(EndlessSpawn());
    }

    private void Spawn()
    {
        Instantiate(m_prefab, m_spawnPlace.position, m_spawnPlace.rotation);
        m_spawnFX.Play();
    }

    private IEnumerator EndlessSpawn()
    {
        yield return new WaitForSeconds(m_startDelay);

        var spawnDelay = new WaitForSeconds(m_spawnFrequency);

        while (true)
        {
            Spawn();

            yield return spawnDelay;
        }
    }
}
