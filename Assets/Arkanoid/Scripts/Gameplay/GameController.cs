﻿using System;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static int PlayerHealth;
    public static bool IsVictory;
    public static bool StopGame;
    public static event Action<bool> EndOfGameEvent;

    private float m_timer = 1f;

    private void Start()
    {
        ResetGameState();
    }

    private void Update()
    {
        if(Time.time < m_timer)
        {
            return;
        }

        m_timer = Time.time + 1f;

        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        var isEndOfGame = PlayerHealth <= 0 || enemies.Length == 0;

        if (isEndOfGame)
        {
            if (PlayerHealth <= 0) IsVictory = false;
            else if (enemies.Length == 0) IsVictory = true;

            if(EndOfGameEvent != null)
            {
                EndOfGameEvent.Invoke(IsVictory);
                enabled = false;
            }
        }
    }

    private void ResetGameState()
    {
        IsVictory = false;
        StopGame = true;
        PlayerHealth = 3;
    }
}
