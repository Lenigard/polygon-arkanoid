﻿using UnityEngine;

public class ManualPlatformMovement : APlatformMovement
{
    protected override Vector3 CalculateVelocity()
    {
        return Vector3.right * CurrSpeed * CustomInput.HorizontalAxis;
    }
}
