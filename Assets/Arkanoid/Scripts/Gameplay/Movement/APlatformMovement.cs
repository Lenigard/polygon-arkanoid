﻿using UnityEngine;

public abstract class APlatformMovement : MonoBehaviour
{
    [Header("Movement:")]
    [SerializeField] private float m_leftBound = -8.5f;
    [SerializeField] private float m_rightBound = 8.5f;
    [SerializeField] private float m_movementSpeed = 0.2f;
    [SerializeField] private float m_lerpSpeed = 8f;

    [Header("Rotation:")]
    [SerializeField] private float m_rotSmoothness = 2f;
    [SerializeField] private float m_maxAngle = 45f;

    [Header("References:")]
    [SerializeField] private Animator m_animator;

    private Vector3 m_currVelocity;

    public float BaseSpeed { get { return m_movementSpeed; } }
    public float CurrSpeed { get; set; }

    protected abstract Vector3 CalculateVelocity();

    private void Start()
    {
        CurrSpeed = BaseSpeed;
    }

    private void Update()
    {
        var targetVelocity = CalculateVelocity();

        //Transformation
        DoMovement(targetVelocity);
        DoRotation(targetVelocity);

        //Animation
        m_animator.SetFloat("Speed", m_currVelocity.magnitude);
    }

    private void DoMovement(Vector3 targetVelocity)
    {
        m_currVelocity = Vector3.Lerp(m_currVelocity, targetVelocity, Time.deltaTime * m_lerpSpeed);
        Vector3 newPos = transform.position + m_currVelocity;
        newPos.x = Mathf.Clamp(newPos.x, m_leftBound, m_rightBound);

        transform.position = newPos;
    }

    private void DoRotation(Vector3 targetVelocity)
    {
        var yAngle = targetVelocity.x / CurrSpeed * m_maxAngle;
        var rot = Quaternion.Euler(0, yAngle, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * m_rotSmoothness);
    }
}