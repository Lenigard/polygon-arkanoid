﻿using UnityEngine;

public class PlatformShooting : MonoBehaviour
{
    [Header("Settings:")]
    [SerializeField] private float m_pushForce = 5f;

    [Header("References:")]
    [SerializeField] private Animator m_animator;
    [SerializeField] private Rigidbody m_ballPrefab;
    [SerializeField] private Transform m_spawnPlace;

    private Rigidbody m_rbBall;

    private void Start()
    {
        TrySpawnBall();

        CustomInput.ShotAction += OnShotBtnDown;
    }

    private void OnDestroy()
    {
        CustomInput.ShotAction -= OnShotBtnDown;
    }

    private void FixedUpdate()
    {
        if (GameController.StopGame)
        {
            ResetBallOnce();
        }
    }

    private void OnShotBtnDown()
    {
        if(m_rbBall.isKinematic)
        {
            m_animator.SetTrigger("ThrowBall");
        }
    }

    public void ResetBallOnce()
    {
        if (!m_rbBall.isKinematic)
        {
            m_rbBall.isKinematic = true;
            m_rbBall.transform.SetParent(m_spawnPlace);
            m_rbBall.transform.SetPositionAndRotation(m_spawnPlace.position, m_spawnPlace.rotation);
        }
    }

    //Calling from animation
    private void TryPushBall()
    {
        if (m_rbBall != null)
        {
            m_rbBall.isKinematic = false;
            m_rbBall.transform.parent = null;
            m_rbBall.AddForce(m_rbBall.transform.forward * m_pushForce, ForceMode.Impulse);

            GameController.StopGame = false;
        }
    }

    private void TrySpawnBall()
    {
        if(m_ballPrefab != null)
        {
            m_rbBall = Instantiate(m_ballPrefab, m_spawnPlace.position, m_spawnPlace.rotation, m_spawnPlace);
            m_rbBall.isKinematic = true;
        }
        else
        {
            Debug.Log("Ball prefab is Empty");
        }
    }
}
